﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelForm.aspx.cs" Inherits="SectionBKhushboo.HotelForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background-color:azure;">
    <form id="form1" runat="server">
            <h1  style="text-align:center;">Welcome To hotel booking Service</h1>
        <div>
            <h2 style="text-align:center;"> Form</h2>
                
                    <div runat="server" id="MyForm">

                    </div>
                    <asp:Label runat="server">Name</asp:Label>
                    <asp:TextBox runat="server" ID="CustomerName" placeholder="Enter your name"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="Validatorname" ControlToValidate="CustomerName" ErrorMessage="You must enter your Name..!">
                    </asp:RequiredFieldValidator>
                    <br />
           
                    <asp:Label runat="server">Email</asp:Label>
                    <asp:TextBox runat="server" ID="CustomerEmail" placeholder="Enter your E-mail"></asp:TextBox>
                    <asp:RegularExpressionValidator runat="server" ID="Validatormail" ControlToValidate="CustomerEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Enter valid E-mail..!">
                    </asp:RegularExpressionValidator>
                    <br />
                        
              
                    <asp:Label runat="server">Phone No</asp:Label>
                    <asp:TextBox runat="server" ID="CustomerPhoneNo" placeholder="Enter your Mobile Number"></asp:TextBox>
                    <asp:RegularExpressionValidator runat="server" ID="Validatorpnumber" ControlToValidate="CustomerPhoneNo" ValidationExpression="[0-9]{10}" ErrorMessage="This isn't valid mobile number..!">
                    </asp:RegularExpressionValidator>
                    <br />

                    <asp:Label runat="server">Adults</asp:Label>
                    <asp:DropDownList runat="server" ID="BookingAdults">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        </asp:DropDownList>
                    <br />
                    
                    <asp:Label runat="server">Childrens</asp:Label>
                    <asp:DropDownList runat="server" ID="Bookingchild">
                    <asp:ListItem>No Children</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    </asp:DropDownList>
                    <br />
              
                    <asp:label runat="server">Type Of Rooms</asp:label>
                    <asp:DropDownList runat="server" ID="BookingRooms">
                        <asp:ListItem>single</asp:ListItem>
                        <asp:ListItem>Double</asp:ListItem>
                        <asp:ListItem>Triple</asp:ListItem>
                        <asp:ListItem>Quad</asp:ListItem>
                        <asp:ListItem>King</asp:ListItem>
                        <asp:ListItem>Twin</asp:ListItem>
                        <asp:ListItem>Studio</asp:ListItem>
                        </asp:DropDownList>
                    <br />
                
                    <asp:Label runat="server">Amenities</asp:Label>
                    <div runat="server" id="Amenities_container">
                     <asp:CheckBox runat="server" ID="BookingGym" Text="GYM" />
                    <asp:CheckBox runat="server" ID="BookingWifi" Text="WIFI" />
                    </div>
                    <br />
                     
                    <asp:Label runat="server">Payment</asp:Label>
                    <asp:RadioButtonList runat="server" ID="PaymentType">
                    <asp:ListItem >Pay Online</asp:ListItem>
                    <asp:ListItem >Pay at  Hotel</asp:ListItem>
                    </asp:RadioButtonList>
                   <asp:RequiredFieldValidator runat="server" ID="pay" ControlToValidate="PaymentType" ErrorMessage="Choose either of them" >
                    </asp:RequiredFieldValidator>
                    <br />
                
                    <asp:Button runat="server"  OnClick="DetailsHere" ID="Sbutton"  Text="Submit" /><br />
                    <asp:ValidationSummary runat="server" ID="ValidSum" HeaderText="You must enter following fields" />
              
                    <div runat="server" id="details">

                    </div>

       
    </form>
</body>
</html>

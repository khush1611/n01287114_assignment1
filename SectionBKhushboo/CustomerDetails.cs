﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SectionBKhushboo
{
    public class CustomerDetails
    {
        private string CustName;
        private string CustEmail;
        private string CustPhoneNo;

        public CustomerDetails()
            {
            }
        public string custName
        {
            get { return CustName; }
            set { CustName = value; }
        }

        public string custEmail
        {
            get { return CustEmail; }
            set {CustEmail = value; }
        }

        public string custPhone
        {
            get { return CustPhoneNo; }
            set { CustPhoneNo = value; }
        }

    }

}
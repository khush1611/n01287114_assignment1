﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SectionBKhushboo
{
    public class PrintRecepit
    {
        public CustomerDetails Customers;
        public BookingDetails Bookings;
        public HotelService Hservices;

        public PrintRecepit(CustomerDetails CD, BookingDetails BD, HotelService HS)
        {
            Customers = CD;
            Bookings = BD;
            Hservices = HS;
        }

        public string MyDetails()
        {
            string Details = "Your Details here: <b> <br/> <br/>";
            Details += "<b>Your total is:</b>  " + CalculateOrder().ToString() + " <br/>";
            Details += "<b>Name:</b>" +Customers.custName+"<br/>";
            Details += "<b>Email:</b>"+Customers.custEmail+"<br/>";
            Details += "<b>Phone No:</b>" +Customers.custPhone+"<br/>";
            Details += "<b>Adults:</b>"+Bookings.BAdults+"<br/>";
            Details += "<b>Child: </b>"+Bookings.BChild+"<br/>";
            Details += "<b>Rooms :</b>"+Bookings.BRooms+"<br/>";
            Details += "<b>Hotel Service:</b>" + string.Join(", ", Hservices.Services.ToArray()) + "</br>";

            return Details;

        }

        public double CalculateOrder()
        {
            double total = 0;

            if (Bookings.BRooms== "single")
            {
                total = 200;
            }
            else if(Bookings.BRooms == "Double")
            {
                total = 400;
            }
            else if (Bookings.BRooms == "Triple")
            {
                total = 600;
            }

            else if (Bookings.BRooms == "Quad")
            {
                total = 800;
            }

            else if (Bookings.BRooms == "King")
            {
                total = 1000;
            }

            else if (Bookings.BRooms == "Twin")
            {
                total = 1200;
            }

            else if (Bookings.BRooms == "Studio")
            {
                total = 1400;
            }
            double sum = Hservices.Services.Count() * 20;
            total += sum;

            return total;

        }

    }
}
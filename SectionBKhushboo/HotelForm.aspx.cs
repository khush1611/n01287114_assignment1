﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SectionBKhushboo
{
    public partial class HotelForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void DetailsHere (object sender, EventArgs e)
        {
            if(!Page.IsValid)
            {
                return;
            }

            MyForm.InnerHtml= "<b>Thank You for booking with us..! </b><br/> <br/>";

    //Creating Customers objects
            string Name = CustomerName.Text.ToString();
            string Email = CustomerEmail.Text.ToString();
            string PhoneNo = CustomerPhoneNo.Text.ToString();

            CustomerDetails newcustomer = new CustomerDetails();

            newcustomer.custName = Name;
            newcustomer.custEmail = Email;
            newcustomer.custPhone = PhoneNo;

   //Creating Booking Details Objects
            int Adults = int.Parse(BookingAdults.SelectedValue);
            string Children =Bookingchild.SelectedItem.Value.ToString();
            string Rooms = BookingRooms.SelectedItem.Value.ToString();

            BookingDetails newbookingDetails = new BookingDetails(Adults,Children,Rooms);

            //Creating  Hotel Serice Objects
           // List<string> Services = new List<string>();

            List<string> h_service = new List<string>();
    
            foreach(Control control in Amenities_container.Controls)
            {
                if(control.GetType()==typeof(CheckBox))
                {
                    CheckBox BookingS = (CheckBox)control;
                    if(BookingS.Checked)
                    {
                        h_service.Add(BookingS.Text); 
                    }

                }
            }
            HotelService newh = new HotelService(h_service);

            //newh.Services = newh.Services.Concat(h_service).ToList();
            PrintRecepit newprintRecepit = new PrintRecepit(newcustomer, newbookingDetails, newh);
            details.InnerHtml = newprintRecepit.MyDetails();

        }
    }
}
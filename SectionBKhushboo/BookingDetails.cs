﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SectionBKhushboo
{
    public class BookingDetails
    {
           // information we are going to add in booking details
           //BookingAdults
           //BookingChild
           //BookingRooms
           //BookingGym
           //BookingWifi
        public int BAdults;
        public string BChild;
        public string BRooms;

        public BookingDetails(int BA, string BC, string BR)
        {
            BAdults =  BA;
            BChild  =  BC;
            BRooms  =  BR;
        }

    }
}